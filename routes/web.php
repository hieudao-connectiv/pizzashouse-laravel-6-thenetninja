<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pizzas', function () {
    // return view('pizzas');                                       // Content-Type: text/html; charset=UTF-8
    // return 'pizzas!';                                            // Content-Type: text/html; charset=UTF-8
    // return ['name' => 'veg pizzas', 'base' => 'classic'];        // Content-Type: application/json

    // get value from db
    $pizza = [
        'type' => 'hawaiian', 
        'base' => 'cheesy crust',
        'price' => 10
    ];
    return view('pizzas', $pizza);
});

// Route::get('role', [TestController::class, 'index'])->middleware('role');

// Route::get('role', [
//     'middleware' => 'Role:editor',
//     // 'uses' => 'App\Http\Controllers\TestController@index'
//     'uses' => [TestController::class, 'index']
// ]);